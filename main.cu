#include <iostream>
#include <fstream>
#include <algorithm>
#include <chrono>

struct GE
{
    uint32_t G;
    float E;
};


int const n = 6; //число спинов в неувеличенном столбце
int const n_incr = n + n/2; //число спинов в увеличенном столбце
int const length = 1<<n; //число конфигураций неувеличенного столбца
int const length_incr = 1<<n_incr; //число конфигураций увеличенного столбца

//обменные интегралы (см. low статью)
float const J1 = -0.002398;
float const J2 = 0.007259;
float const J3 = -0.001323;
//float const J4 = -0.003422;
__device__ float const dev_J1 = -0.002398;
__device__ float const dev_J2 = 0.007259;
__device__ float const dev_J3 = -0.001323;
__device__ float const dev_J4 = -0.003422;

#define CUDA_CHECK_ERROR(err)           \
if ((err) != cudaSuccess)               \
{          \
    printf("Cuda error: %s\n", cudaGetErrorString(err));    \
    printf("Error in file: %s, line: %i\n", __FILE__, __LINE__); \
    return 1;\
}

int get_SP_cores(cudaDeviceProp devProp)
{
    int cores = 0;
    int mp = devProp.multiProcessorCount;
    switch (devProp.major){
        case 2: // Fermi
            if (devProp.minor == 1) cores = mp * 48;
            else cores = mp * 32;
            break;
        case 3: // Kepler
            cores = mp * 192;
            break;
        case 5: // Maxwell
            cores = mp * 128;
            break;
        case 6: // Pascal
            if ((devProp.minor == 1) || (devProp.minor == 2)) cores = mp * 128;
            else if (devProp.minor == 0) cores = mp * 64;
            else printf("Unknown device type\n");
            break;
        case 7: // Volta and Turing
            if ((devProp.minor == 0) || (devProp.minor == 5)) cores = mp * 64;
            else printf("Unknown device type\n");
            break;
        case 8: // Ampere
            if (devProp.minor == 0) cores = mp * 64;
            else if (devProp.minor == 6) cores = mp * 128;
            else printf("Unknown device type\n");
            break;
        default:
            printf("Unknown device type\n");
            break;
    }
    return cores;
}

void thread_init(int8_t *chain, int configs, int spins)
{
    for (auto i=0; i<configs; i++)
    {
        int bit = i;
        for (auto j=0; j<spins; ++j)
        {
            chain[i * spins + j] = bit & 1 ? 1 : -1;
            bit>>=1;
        }
    }
}

void generator_ge_o_I(GE* ge_o, const int8_t *chain)
{
    for(auto i=0; i<length_incr; ++i)
    {
        ge_o[i].E = 0;
        //первый треугольник
        ge_o[i].E += -J1 * (float)chain[i * n_incr + 0] * (float)chain[i * n_incr + 1];
        ge_o[i].E += -J1 * (float)chain[i * n_incr + 2] * (float)chain[i * n_incr + 1];
        ge_o[i].E += -J3 * (float)chain[i * n_incr + 0] * (float)chain[i * n_incr + 2];
        //последовательная часть
        ge_o[i].E += -J2 * (float)chain[i * n_incr + 2] * (float)chain[i * n_incr + 3];
        ge_o[i].E += -J1 * (float)chain[i * n_incr + 3] * (float)chain[i * n_incr + 4];
        ge_o[i].E += -J1 * (float)chain[i * n_incr + 4] * (float)chain[i * n_incr + 5];
        //второй треугольник
        ge_o[i].E += -J1 * (float)chain[i*n_incr + 6] * (float)chain[i * n_incr + 7];
        ge_o[i].E += -J1 * (float)chain[i*n_incr + 8] * (float)chain[i * n_incr + 7];
        ge_o[i].E += -J3 * (float)chain[i*n_incr + 6] * (float)chain[i * n_incr + 8];
        ge_o[i].G = 1;
    }
}

void generator_ge_o_II(GE* ge_o, const int8_t *chain)
{
    for(auto i=0; i<length; ++i)
    {
        ge_o[i].E = 0;
        //первая партия
        ge_o[i].E += -J3 * (float)chain[i * n + 0] * (float)chain[i * n + 1];
        ge_o[i].E += -J2 * (float)chain[i * n + 1] * (float)chain[i * n + 2];
        //вторая партия
        ge_o[i].E += -J2 * (float)chain[i * n + 3] * (float)chain[i * n + 4];
        ge_o[i].E += -J3 * (float)chain[i * n + 4] * (float)chain[i * n + 5];
        ge_o[i].G = 1;
    }
}

void generator_ge_o_III(GE* ge_o, const int8_t *chain)
{
    for(auto i=0; i<length_incr; ++i)
    {
        ge_o[i].E = 0;
        //последовательная часть
        ge_o[i].E += -J1 * (float)chain[i * n_incr + 0] * (float)chain[i * n_incr + 1];
        ge_o[i].E += -J1 * (float)chain[i * n_incr + 1] * (float)chain[i * n_incr + 2];
        ge_o[i].E += -J2 * (float)chain[i * n_incr + 2] * (float)chain[i * n_incr + 3];
        //первый треугольник
        ge_o[i].E += -J1 * (float)chain[i * n_incr + 3] * (float)chain[i * n_incr + 4];
        ge_o[i].E += -J1 * (float)chain[i * n_incr + 5] * (float)chain[i * n_incr + 4];
        ge_o[i].E += -J3 * (float)chain[i * n_incr + 3] * (float)chain[i * n_incr + 5];
        //последовательная часть
        ge_o[i].E += -J2 * (float)chain[i * n_incr + 5] * (float)chain[i * n_incr + 6];
        ge_o[i].E += -J1 * (float)chain[i * n_incr + 6] * (float)chain[i * n_incr + 7];
        ge_o[i].E += -J1 * (float)chain[i * n_incr + 7] * (float)chain[i * n_incr + 8];
        ge_o[i].G = 1;
    }
}

void generator_ge_o_IV(GE* ge_o, const int8_t *chain)
{
    for(auto i=0; i<length; ++i)
    {
        ge_o[i].E = 0;
        //первая партия
        ge_o[i].E += -J2 * (float)chain[i * n + 1] * (float)chain[i * n + 2];
        ge_o[i].E += -J3 * (float)chain[i * n + 2] * (float)chain[i * n + 3];
        ge_o[i].E += -J2 * (float)chain[i * n + 3] * (float)chain[i * n + 4];
        ge_o[i].G = 1;
    }
}

void compression(GE* ge, int lines, int columns, int* elements)
{
    for(auto i=0; i<lines; ++i)
    {
        for(auto j=0; j<columns; ++j)
        {
            for(auto k=j+1; k<columns; ++k)
            {
                if(ge[i * columns + j].E == ge[i * columns + k].E &&
                   ge[i * columns + j].G !=0)
                {
                    ge[i * columns + j].G += ge[i * columns + k].G;
                    ge[i * columns + k].G = 0;
                }
            }
        }
        elements[i] = 0;
        for(auto j=0; j<columns; ++j)
        {
            if(ge[i * columns + j].G != 0) ++elements[i];
        }
        for(auto j=0; j<columns; ++j)
        {
            if(ge[i * columns + j].G == 0)
            {
                for (auto k = j + 1; k < columns; ++k)
                {
                    if(ge[i * columns + k].G != 0)
                    {
                        ge[i * columns + j].G = ge[i * columns + k].G;
                        ge[i * columns + j].E = ge[i * columns + k].E;
                        ge[i * columns + k].G = 0;
                        break;
                    }
                }
            }
        }
    }
}

extern __shared__ GE ge_shared[];

__global__ void sort(GE* ge, int columns, int i)
{
    auto x = blockIdx.x * blockDim.x + threadIdx.x;
    ge_shared[threadIdx.x].G = 0;
    ge_shared[threadIdx.x].E = 0;
    __syncthreads();
    for(auto j = x; j < columns; j += blockDim.x * gridDim.x)
    {
        ge_shared[threadIdx.x] = ge[i * columns + j];
        __syncthreads();
        for (auto k = 1; k < blockDim.x; ++k)
        {
            if ((threadIdx.x % (2 * k) < (k - 1)) && ge_shared[threadIdx.x].G != 0 &&
                (threadIdx.x + k) < blockDim.x)
            {
                if (ge_shared[threadIdx.x].E == ge_shared[threadIdx.x + k].E)
                {
                    ge_shared[threadIdx.x].G += ge_shared[threadIdx.x + k].G;
                    ge_shared[threadIdx.x + k].G = 0;
                }
            }
            __syncthreads();
        }
        for(auto k = 1; k < blockDim.x; ++k)
        {
            if((threadIdx.x % (2 * k) < (k - 1)) && ge_shared[threadIdx.x].G == 0 && (threadIdx.x + k) < blockDim.x)
            {
                if(ge_shared[threadIdx.x + k].G != 0)
                {
                    ge_shared[threadIdx.x].G = ge_shared[threadIdx.x + k].G;
                    ge_shared[threadIdx.x].E = ge_shared[threadIdx.x + k].E;
                    ge_shared[threadIdx.x + k].G = 0;
                }
            }
            __syncthreads();
        }
        ge[i * columns + j] = ge_shared[threadIdx.x];
    }
}

__global__ void compression(GE* ge_IV, int columns, int line)
{
    __shared__ bool out;
    auto read_idx = line + threadIdx.x;
    ge_shared[threadIdx.x] = ge_IV[read_idx];
    unsigned int j = 0; //write index
    unsigned int k = blockDim.x; //read index
    while(k < columns)
    {
        if(threadIdx.x == 0) out = true;
        __syncthreads();
        /****************************** read data *********************************************************************/
        while(out && k < columns)
        {
            if(threadIdx.x == 0) out = false;
            __syncthreads();
            for(auto l = 0; l < blockDim.x; ++l)
            {
                read_idx = line + k + (threadIdx.x + l) % blockDim.x;
                if(ge_shared[threadIdx.x].G == 0 && ge_IV[read_idx].G != 0 && (read_idx - line) < columns)
                {
                    ge_shared[threadIdx.x] = ge_IV[read_idx];
                    ge_IV[read_idx].G = 0;
                }
                __syncthreads();
            }
            if(ge_shared[threadIdx.x].G == 0) out = true;
            __syncthreads();
            if(out) k += blockDim.x;
        }
        /****************************** write data*********************************************************************/
        if(j + threadIdx.x < columns) ge_IV[line + j + threadIdx.x] = ge_shared[threadIdx.x];
        j += blockDim.x;
        ge_shared[threadIdx.x].G = 0;
        ge_shared[threadIdx.x].E = 0;
        __syncthreads();
    }
}

__global__ void counter(GE* ge_IV, int columns, int line, int i, int* elements)
{
    for(int j = columns - 1; j >= 0; --j)
    {
        if(ge_IV[line + j].G != 0)
        {
            elements[i] = j;
            break;
        }
    }
}

__global__ void unifier_ge_II(GE* ge_II, GE* ge_I, GE* ge_o_II, const int8_t *chain, const int8_t *chain_incr,
                              const int* elements_previous, int max_elements_this, int max_elements_previous, int i)
{
    for(auto j = blockIdx.x; j < length_incr; j += gridDim.x) //перебор по конфигурациям левой цепочки
    {
        for(auto k = threadIdx.x; k < elements_previous[j]; k += blockDim.x) //перебор по энергиям определенной конфигурации левой цепочки
        {
            if(ge_I[j].G != 0)
            {
                ge_II[i * length_incr * max_elements_this + j * max_elements_this + k].G = ge_I[j * max_elements_previous + k].G;
                ge_II[i * length_incr * max_elements_this + j * max_elements_this + k].E = ge_I[j * max_elements_previous + k].E + ge_o_II[i].E;
                //тройка
                ge_II[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float) chain_incr[j * n_incr + 1] * (float) chain[i * n + 0];
                ge_II[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float) chain_incr[j * n_incr + 1] * (float) chain[i * n + 1];
                //пятерка
                ge_II[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J3 * (float) chain_incr[j * n_incr + 3] * (float) chain[i * n + 2];
                ge_II[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float) chain_incr[j * n_incr + 4] * (float) chain[i * n + 2];
                ge_II[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float) chain_incr[j * n_incr + 4] * (float) chain[i * n + 3];
                ge_II[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J3 * (float) chain_incr[j * n_incr + 5] * (float) chain[i * n + 3];
                //тройка
                ge_II[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float) chain_incr[j * n_incr + 7] * (float) chain[i * n + 4];
                ge_II[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float) chain_incr[j * n_incr + 7] * (float) chain[i * n + 5];
            }
        }
    }
}

__global__ void unifier_ge_III(GE* ge_III, GE* ge_II, GE* ge_o_III, const int8_t *chain, const int8_t *chain_incr,
                                   const int*elements_previous, int max_elements_this, int max_elements_previous, int i)
{
    for(auto j = blockIdx.x; j < length; j += gridDim.x) //перебор по конфигурациям левой цепочки
    {
        for(auto k = threadIdx.x; k < elements_previous[j]; k += blockDim.x) //перебор по энергиям определенной конфигурации левой цепочки
        {
            if(ge_II[j * length_incr * max_elements_previous + k].G != 0)
            {
                ge_III[i * length * max_elements_this + j * max_elements_this + k].G = ge_II[j * length_incr * max_elements_previous + k].G;
                ge_III[i * length * max_elements_this + j * max_elements_this + k].E = ge_II[j * length_incr * max_elements_previous + k].E + ge_o_III[i].E;
                //верхняя пара
                ge_III[i * length * max_elements_this + j * max_elements_this + k].E += -dev_J2 * (float) chain[j * n + 0] * (float) chain_incr[i * n_incr + 0];
                //первый крест
                ge_III[i * length * max_elements_this + j * max_elements_this + k].E += -dev_J2 * (float) chain[j * n + 1] * (float) chain_incr[i * n_incr + 2];
                ge_III[i * length * max_elements_this + j * max_elements_this + k].E += -dev_J2 * (float) chain[j * n + 2] * (float) chain_incr[i * n_incr + 3];
                ge_III[i * length * max_elements_this + j * max_elements_this + k].E += -dev_J4 * (float) chain[j * n + 1] * (float) chain_incr[i * n_incr + 3];
                ge_III[i * length * max_elements_this + j * max_elements_this + k].E += -dev_J4 * (float) chain[j * n + 2] * (float) chain_incr[i * n_incr + 2];
                //второй крест
                ge_III[i * length * max_elements_this + j * max_elements_this + k].E += -dev_J2 * (float) chain[j * n + 3] * (float) chain_incr[i * n_incr + 5];
                //нижняя пара
                ge_III[i * length * max_elements_this + j * max_elements_this + k].E += -dev_J2 * (float) chain[j * n + 5] * (float) chain_incr[i * n_incr + 8];
            }
        }
    }
}

__global__ void unifier_ge_IV(GE* ge_IV, GE* ge_III, GE* ge_o_IV, const int8_t *chain, const int8_t *chain_incr,
                                  const int* elements_previous, int max_elements_this, int max_elements_previous, int i)
{
    for(auto j = blockIdx.x; j < length_incr; j += gridDim.x) //перебор по конфигурациям левой цепочки
    {
        for(auto k = threadIdx.x ; k < elements_previous[j]; k += blockDim.x) //перебор по энергиям определенной конфигурации левой цепочки
        {
            if(ge_III[j * length * max_elements_previous + k].G != 0)
            {
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].G = ge_III[j * length * max_elements_previous + k].G;
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E = ge_III[j * length * max_elements_previous + k].E + ge_o_IV[i].E;
                //пятерка
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J3 * (float)chain_incr[j * n_incr + 0] * (float)chain[i * n + 0];
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float)chain_incr[j * n_incr + 1] * (float)chain[i * n + 0];
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float)chain_incr[j * n_incr + 1] * (float)chain[i * n + 1];
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J3 * (float)chain_incr[j * n_incr + 2] * (float)chain[i * n + 1];
                //тройка
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float)chain_incr[j * n_incr + 4] * (float)chain[i * n + 2];
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float)chain_incr[j * n_incr + 4] * (float)chain[i * n + 3];
                //пятерка
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J3 * (float)chain_incr[j * n_incr + 6] * (float)chain[i * n + 4];
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float)chain_incr[j * n_incr + 7] * (float)chain[i * n + 4];
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J1 * (float)chain_incr[j * n_incr + 7] * (float)chain[i * n + 5];
                ge_IV[i * length_incr * max_elements_this + j * max_elements_this + k].E += -dev_J3 * (float)chain_incr[j * n_incr + 8] * (float)chain[i * n + 5];
            }
        }
    }
}

void outputs(GE* ge_output, const int lines, const int columns)
{
    std::ofstream file_output("GE_output.txt");
    std::ofstream file_out("GE_out.dat");
    uint32_t G_sum = 0;
    auto ge_out = new GE[lines * columns];
    int max_k = 0;
    for(auto i=0; i<lines; ++i)
    {
        for(auto j=0; j<columns; ++j)
        {
            if(ge_output[i * columns + j].G != 0)
            {
                file_output << "G=" << ge_output[i * columns + j].G
                            << " E=" << ge_output[i * columns + j].E;
                file_output << " ";
                G_sum += ge_output[i * columns + j].G;
                for(auto k=0; k<columns * j; ++k)
                {
                    if(ge_out[k].G == 0)
                    {
                        ge_out[k].G = ge_output[i*columns+j].G;
                        ge_out[k].E = ge_output[i*columns+j].E;
                        if(max_k < k) max_k = k;
                        break;
                    }
                    if(ge_out[k].E == ge_output[i*columns+j].E)
                    {
                        ge_out[k].G += ge_output[i*columns+j].G;
                        break;
                    }
                }
            }
        }
        file_output << "\n \n";
    }
    file_out << max_k << " ";
    for(auto i=0; i<lines*columns; ++i)
    {
        if(ge_out[i].G == 0) break;
        file_out << ge_out[i].G << " "
                 << ge_out[i].E << " ";
    }
    file_output << "Sum of G = " << G_sum << "\n";
    std::cout << "Sum of G = " << G_sum << "\n";
    delete [] ge_out;
}

int main()
{
    auto t1 = std::chrono::high_resolution_clock::now();

    /***************************************** GPU configuration ******************************************************/

    cudaDeviceProp dev{};
    cudaGetDeviceProperties(&dev, 0);
    static size_t block_dim = 512;
    static size_t grid_dim = get_SP_cores(dev);
    static size_t shared_size = block_dim * sizeof(GE) + block_dim * sizeof(uint32_t) + sizeof(bool);
    std::cout << "sp_cores: " << get_SP_cores(dev) << "\n";

    /***************************************** base creations *********************************************************/

    int8_t *chain, *chain_incr;
    CUDA_CHECK_ERROR(cudaMallocManaged(&chain, length * n * sizeof(int8_t)))
    thread_init(chain, length, n);
    CUDA_CHECK_ERROR(cudaMallocManaged(&chain_incr, length_incr * n * sizeof(int8_t)))
    thread_init(chain_incr, length_incr, n_incr);
    GE *ge_o_I, *ge_o_II, *ge_o_III, *ge_o_IV;
    CUDA_CHECK_ERROR(cudaMallocManaged(&ge_o_I, length_incr * sizeof(GE)))
    CUDA_CHECK_ERROR(cudaMallocManaged(&ge_o_II, length * sizeof(GE)))
    CUDA_CHECK_ERROR(cudaMallocManaged(&ge_o_III, length_incr * sizeof(GE)))
    CUDA_CHECK_ERROR(cudaMallocManaged(&ge_o_IV, length * sizeof(GE)))
    int *elements, *elements_incr;
    CUDA_CHECK_ERROR(cudaMallocManaged(&elements, length * sizeof(int)))
    CUDA_CHECK_ERROR(cudaMallocManaged(&elements_incr, length_incr * sizeof(int)))
    generator_ge_o_I(ge_o_I, chain_incr);
    generator_ge_o_II(ge_o_II, chain);
    generator_ge_o_III(ge_o_III, chain_incr);
    generator_ge_o_IV(ge_o_IV, chain);

    /***************************************** 2st unifying ***********************************************************/

    GE *ge_II;
    CUDA_CHECK_ERROR(cudaMallocManaged(&ge_II, length * length_incr * sizeof(GE)))
    for(auto i = 0; i < length_incr; ++i)
    {
        elements_incr[i] = 1;
    }
    for(auto i = 0; i < length; ++i)
    {
        unifier_ge_II<<<grid_dim, block_dim>>>(ge_II, ge_o_I, ge_o_II, chain, chain_incr,
                                               elements_incr, 1, 1, i);
    }
    cudaDeviceSynchronize();
    compression(ge_II, length, length_incr, elements);
    int max_elements = *(std::max_element(&elements[0], &elements[length]));

    /****************************************** 3nd unifying **********************************************************/

    GE *ge_III;
    CUDA_CHECK_ERROR(cudaMallocManaged(&ge_III, length_incr * length * max_elements * sizeof(GE)))
    for (auto i = 0; i < length_incr; ++i)
    {
        unifier_ge_III<<<grid_dim, block_dim>>>(ge_III, ge_II, ge_o_III, chain, chain_incr,
                                                    elements, max_elements,
                                                    1, i);
        sort<<<grid_dim, block_dim, shared_size>>>(ge_III,max_elements * length, i);
        compression<<<1, block_dim, shared_size>>>(ge_III,max_elements * length,
                                                   i * max_elements * length);
        counter<<<1, 1>>>(ge_III,max_elements * length,i * max_elements * length, i,
                                    elements_incr);
    }
    cudaDeviceSynchronize();
    //compression(ge_III, length_incr, max_elements*length, elements_incr);
    int max_elements_incr = *(std::max_element(&elements_incr[0], &elements_incr[length_incr]));
    std::cout << "columns in IV: " << max_elements_incr * length_incr << "\n";

    /****************************************** 4th unifying **********************************************************/

    GE *ge_IV;
    CUDA_CHECK_ERROR(cudaMallocManaged(&ge_IV, length * length_incr * max_elements_incr * sizeof(GE)))
    for (auto i = 0; i < length; ++i)
    {
        unifier_ge_IV<<<grid_dim, block_dim>>>(ge_IV, ge_III, ge_o_IV, chain, chain_incr,
                                                                   elements_incr,
                                                                   max_elements_incr,
                                                                   max_elements, i);
        sort<<<grid_dim, block_dim, shared_size>>>(ge_IV,
                                                              max_elements_incr * length_incr, i);
        compression<<<1, block_dim, shared_size>>>(ge_IV,
                                                         max_elements_incr * length_incr,
                                                         i * max_elements_incr * length_incr);
        counter<<<1, 1>>>(ge_IV,max_elements_incr * length_incr,
                                       i * max_elements_incr * length_incr, i, elements);
    }
    cudaDeviceSynchronize();
    max_elements = *(std::max_element(&elements[0], &elements[length]));
    std::cout << "columns in V: " << max_elements * length << "\n";

    /************************************* Output and memory cleaning *************************************************/

    //outputs(ge_IV, length, max_elements_incr * length_incr); // выводимый массив, строки(*ge), столбцы в строке (ge)
    std::ofstream work_time("time.txt");
    CUDA_CHECK_ERROR(cudaFree(chain))
    CUDA_CHECK_ERROR(cudaFree(chain_incr))
    CUDA_CHECK_ERROR(cudaFree(ge_o_I))
    CUDA_CHECK_ERROR(cudaFree(ge_o_II))
    CUDA_CHECK_ERROR(cudaFree(ge_o_III))
    CUDA_CHECK_ERROR(cudaFree(elements))
    CUDA_CHECK_ERROR(cudaFree(ge_o_IV))
    CUDA_CHECK_ERROR(cudaFree(elements_incr))
    CUDA_CHECK_ERROR(cudaFree(ge_II))
    CUDA_CHECK_ERROR(cudaFree(ge_III))
    CUDA_CHECK_ERROR(cudaFree(ge_IV))
    auto t2 = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
    std::cout << "working time is " << time / 3600000 << " h " << (time % 3600000) / 60000 << " m "
              << ((time % 3600000) % 60000) / 1000 << " s " << ((time % 3600000) % 60000) % 1000 << " ms \n";
    work_time << "working time is " << time / 3600000 << " h " << (time % 3600000) / 60000 << " m "
              << ((time % 3600000) % 60000) / 1000 << " s " << ((time % 3600000) % 60000) % 1000 << " ms \n";
}